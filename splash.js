/**
 *	Domain Spray
 *		a) Serve - to gather pool.com expired domain data and load the que service 
 */
var AWS = require('aws-sdk'),
	awsCredentialsPath = './config.json',
	sqsQueueUrl = 'https://sqs.us-west-2.amazonaws.com/426593718780/Domainomatics',
	sqs,
	async = require('async'),
	csv = require('fast-csv'),
	unzip = require('unzip'),
	http = require('http'),
	fs = require('fs');

var total = 0;

AWS.config.loadFromPath(awsCredentialsPath);
sqs = new AWS.SQS();

//sqs.createQueue();
//sqs.purgeQueue();
//sqs.sendMessage

function downloadPool(callback) {
	var file = fs.createWriteStream("list.zip");
	var request = http.get("http://www.pool.com/Downloads/PoolDeletingDomainsList.zip",function(response) {
		console.log('Pool file downloading');
		response.pipe(file);
		file.on('finish', function() {
			file.close(callback);
		});
	});
}

function unzipPool() {
	fs.createReadStream('list.zip')
	.pipe(unzip.Parse())
	.on('entry', function(entry) {
		var fileName = entry.path;
		var type = entry.type;
		var size = entry.size;
		console.log('Unzipping pool.');
		entry.pipe(fs.createWriteStream('list.csv'));			
	})
	.on('finish',function() {
		injectQue();
	});
}

function injectQue() {
	console.log('Injecting into work queue.');
	var count=0;
	csv.fromPath('list.csv')
		.on('data',function(data) {
			var domain = data[0];
			sqs.sendMessage({
				MessageBody:domain,
				QueueUrl:sqsQueueUrl,
			}, function(err,data) {
				count++;
				if (err) console.log(err,err.stack);
				else console.log(count);
			});
		})
		.on('end',function() {
			fs.unlink('list.zip');
			fs.unlink('list.csv');
			console.log('Done.');			
		});
}

function splash(callback) {
	downloadPool( unzipPool );
}


splash();
