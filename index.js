/**
 *	Domain Spray Client
 *		a) Client - to read from the queue
 *
 */
var AWS = require('aws-sdk'),
awsCredentialsPath = './config.json',
sqsQueueUrl = 'https://sqs.us-west-2.amazonaws.com/426593718780/Domainomatics',
sqs,
request = require('request'),
async = require('async'),
di = require('../domaininspector');

AWS.config.loadFromPath(awsCredentialsPath);
sqs = new AWS.SQS();

function tick() {
	sqs.receiveMessage({
		QueueUrl: sqsQueueUrl,
		MaxNumberOfMessages: 1, // how many messages do we wanna retrieve?
		VisibilityTimeout: 60, // seconds - how long we want a lock on this job
		WaitTimeSeconds: 3 // seconds - how long should we wait for a message?
	}, function(err, data) {
		if (data.Messages) {
			var message = data.Messages[0];
			console.log(message.Body);
			di.detective(message.Body, function(domainData) {
				request.post(
					'http://www.domainomatics.com/inject.php',
					{ form : domainData },
					function (error, response, body) {
						if (!error && response.statusCode == 200) {
						} else {
							console.log('Error : '+body);
						}
					}
				);
				removeFromQueue(message);  
				tick();
			});
		}
	});
}

var removeFromQueue = function(message) {
	sqs.deleteMessage({
		QueueUrl: sqsQueueUrl,
		ReceiptHandle: message.ReceiptHandle
	}, function(err, data) {
		// If we errored, tell us that we did
		err && console.log(err);
	});
};

console.log('Worker initiated.');
tick();
